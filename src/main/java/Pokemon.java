public class Pokemon {
    private int ataque;
    private int fuerza;
    private String nombre;
    private String naturaleza;

    public Pokemon(int ataque, int fuerza, String nombre, String naturaleza) {
        this.ataque = ataque;
        this.fuerza = fuerza;
        this.nombre = nombre;
        this.naturaleza = naturaleza;
    }

    public int getAtaque() {
        return ataque;
    }

    public int getFuerza() {
        return fuerza;
    }

    public String getNombre() {
        return nombre;
    }

    public String getNaturaleza() {
        return naturaleza;
    }
}