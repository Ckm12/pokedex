import java.util.HashMap;
import java.util.Map;

// Clase Pokedex que utiliza los patrones de diseño Builder, Singleton y Prototype
public class Pokedex {
    // Patrón Singleton: se garantiza que solo haya una instancia de la Pokedex
    private static Pokedex instance = null;
    private Map<String, Pokemon> pokemonMap;

    private Pokedex() {
        this.pokemonMap = new HashMap<>();
    }

    public static Pokedex getInstance() {
        if (instance == null) {
            instance = new Pokedex();
        }
        return instance;
    }

    // Patrón Builder: se utiliza para crear objetos Pokemon de manera flexible
    public static class PokemonBuilder {
        private int ataque;
        private int fuerza;
        private String nombre;
        private String naturaleza;

        public PokemonBuilder setAtaque(int ataque) {
            this.ataque = ataque;
            return this;
        }

        public PokemonBuilder setFuerza(int fuerza) {
            this.fuerza = fuerza;
            return this;
        }

        public PokemonBuilder setNombre(String nombre) {
            this.nombre = nombre;
            return this;
        }

        public PokemonBuilder setNaturaleza(String naturaleza) {
            this.naturaleza = naturaleza;
            return this;
        }

        public Pokemon build() {
            return new Pokemon(ataque, fuerza, nombre, naturaleza);
        }
    }

    // Patrón Prototype: se utiliza para crear copias de un objeto Pokemon existente
    public Pokemon getPokemon(String nombre) {
        Pokemon original = pokemonMap.get(nombre);
        if (original == null) {
            return null;
        }
        return new Pokemon(original.getAtaque(), original.getFuerza(), original.getNombre(), original.getNaturaleza());
    }

    public void addPokemon(String nombre, Pokemon pokemon) {
        pokemonMap.put(nombre, pokemon);
    }
}