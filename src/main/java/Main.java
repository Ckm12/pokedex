

// Ejemplo de uso
public class Main {
    public static void main(String[] args) {
        Pokedex pokedex = Pokedex.getInstance();

        // Añadir 10 pokemons a la Pokedex
        pokedex.addPokemon("Bulbasaur", new Pokemon(49, 49, "Bulbasaur", "Planta"));
        pokedex.addPokemon("Charmander", new Pokemon(52, 43, "Charmander", "Fuego"));
        pokedex.addPokemon("Squirtle", new Pokemon(48, 65, "Squirtle", "Agua"));
        pokedex.addPokemon("Pikachu", new Pokemon(55, 40, "Pikachu", "Eléctrico"));
        pokedex.addPokemon("Jigglypuff", new Pokemon(45, 20, "Jigglypuff", "Normal"));

    }}